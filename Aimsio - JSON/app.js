function loadPicture(url, container) {
    $.getJSON(url, function(data) {
        if (typeof data === 'object') {
            $.each(data['images'], function(key, image) {
                var img = '<li><img src="' + image['url'] + '" alt="' + image['title'] + '"></li>';
                $(container).append(img);
            });
            $(container).anythingSlider();
        }
    });
}

$(function() {
    loadPicture('data.json', '#image1');
});