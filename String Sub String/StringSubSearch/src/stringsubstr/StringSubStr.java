
package stringsubstr;

public class StringSubStr {

    public static void main(String[] args) {
       System.out.println("Checking if substring exists in string..."); //utilizing indexOf()
       String input = "The easiest programming language is C++";
       boolean exists = input.indexOf("programming") != -1? true : false; //specify the substring you are looking for 
    
    
    if(exists){
      System.out.println("May have found something..");
      System.out.println("Found? " + "Yes\n\n");   
      }
    else {
        System.out.println("Can't find what you are looking for\n\n");
    }
    
    //5 test cases with different input and corresponding outputs 
     System.out.println("Testing..Looking for substrings in specified strings...");
     input = "This is another test"; //#1
     boolean isFound = input.contains("test");
     if(isFound){
         System.out.println("Found");
     }
     else{
          System.out.println("Did not find " + "'test'");
      }
     
     input = "Please try again"; //#2
     boolean isFound1 = input.contains("is");
     if(isFound1){
         System.out.println("Found");
     }
     else{
          System.out.println("Did not find " + "'is'");
      }
     
     input = "It is summer"; //#3
     boolean isFound2 = input.contains("summertime");
     if(isFound2){
         System.out.println("Found");
     }
     else{
          System.out.println("Did not find " + "'summertime'");
      }
     
     input = "Calgary Alerta"; //#4
     boolean isFound3  = input.contains("Calgary");
     if(isFound3){
         System.out.println("Found");
     }
     else{
          System.out.println("Did not find " + "'Calgary'");
      }
     
     input = "Calgary Alerta"; //#5
     boolean isFound4  = input.contains("Vacouver");
     if(isFound4){
         System.out.println("Found");
     }
     else{
          System.out.println("Did not find " + "'Vancouver'");
      }
     
    
    }
}
