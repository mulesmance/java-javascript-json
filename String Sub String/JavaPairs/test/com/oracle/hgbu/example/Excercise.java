package com.oracle.hgbu.example;

import static org.junit.Assert.*;
import org.junit.Test;

public class Excercise {
    
    @Test
    public void TestPairs() {
        Excercise ex = new Excercise(); 
        assertEquals(10,(2+8));
    }     
    
    @Test
    public void TestPairs1() {
        Excercise ex = new Excercise(); 
        assertEquals(10,(1+8));
    }     
    
    @Test
    public void TestPairs2() {
        Excercise ex = new Excercise(); 
        assertEquals(10,(2+3));
    }     
    
    @Test
    public void TestPairs3() {
        Excercise ex = new Excercise(); 
        assertEquals(10,(5+7));
    }     
    
    @Test
    public void TestPairs4() {
        Excercise ex = new Excercise(); 
        assertEquals(10,(7+3));
    }     
    
}
