package excercise;

public class Excercise {

   public Excercise(){} // constructor 
   public static void main(String args[]) {
       int[] input = {1, 8, 2, 3, 5, 7};
       sampleOutput(input, 10);
       
    }
    
    public static void getPairs(int[] array, int sum){
    
        //for loop to examine every possible pair 
        for(int i = 0; i < array.length; i++) {
            int first = array[i];
            for(int j = i + 1; j < array.length; j++){
                int second = array[j];
                
                //if the sum of pair is equal to specified sum, print it 
                if((first + second) == sum) {
                System.out.printf("(%d, %d)  %n", first, second);
                }
            }
            
        }
    }
    
    public static void sampleOutput(int[] inputArray, int  sumSpecified) {
        System.out.println("Pairs whose sum is equal to: " + sumSpecified);
        getPairs(inputArray, sumSpecified);
    }
    
}
