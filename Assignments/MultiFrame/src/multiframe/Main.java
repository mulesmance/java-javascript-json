/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package multiframe;


/**
 *
 * @author s0000041
 */
public class Main {

    Frame1 frame1;
    Frame2 frame2;
    Frame3 frame3;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main main = new Main();
    }

    public Main() {
        frame1 = new Frame1(this);
        frame2 = new Frame2(this);
        frame3 = new Frame3(this);

        frame1.setVisible(true);
    }

    public void show1() {
        frame1.setVisible(true);
        frame2.setVisible(false);
        frame3.setVisible(false);
    }

    public void show2() {
        frame1.setVisible(false);
        frame2.setVisible(true);
        frame3.setVisible(false);
    }

    public void show3() {
        frame1.setVisible(false);
        frame2.setVisible(false);
        frame3.setVisible(true);
    }

    public String getData1() {
        return frame1.getData();
    }

    public String getData2() {
        return frame2.getData();
    }
}
