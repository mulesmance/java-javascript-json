/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author s0474086
 */
public class JavaApplication1 {
   
    
   static int maxValue = 0 ;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello world");
        System.out.print(maxValue);
    
        byte small = maxValue;   
        int big = small; 
        
        double large = 12.0;
        float medium = 6.0f;  
        
        Byte first = small;  //boxing primitive to an object
        small = first; //unboxing the object and assigning value to a primitive 
        small = first.byteValue();
        
        
        small = medium;
        first = new Float(medium).byteValue();
        
        
        small = small >> 4; 
    }
}
