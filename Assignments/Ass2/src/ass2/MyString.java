/*
 * Assignment # 2
 * November 4th, 2015
 */
package ass2;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class MyString {
//1. static method
protected String aString;
    
    public MyString(String newString){
        aString = newString;
};
    public static void isSubString(String s1, String s2) {
        if (s2.contains(s1) == true) {
            System.out.println(s1 + "        True");  //will print true if the string is found
        } else {
            System.out.println(s1 + "        False");  //else will print false 
        }
    }
// 2. none-static method
    public boolean isSubString(String s1) {
        if (aString.contains(s1)) {
            System.out.println(s1 + "       True");
        } else {
            System.out.println(s1 + "       False");
        }
        return true;
    }

    //3. Palindrome 
   public static void Palindrome(String a) {
        a = a.toLowerCase();
        a = a.replaceAll("[^a-zA-Z0-9]", "");
        int l = a.length();
        char[] temp = new char[l];
        char[] o = new char[l];

        for (int i = 0; i < l; i++) {
            temp[i] = a.charAt(i);
        }

        for (int i = 0; i < l; i++) {
            o[i] = temp[l - 1 - i];
        }

        String reverse = new String(o);
        boolean outcome;
        outcome = reverse.equals(a);
        System.out.println(outcome);
    }

    public static void Palindrome(int a) {
        String x = String.valueOf(a);
        int l = x.length();
        char[] temp = new char[l];
        char[] o = new char[l];

        for (int i = 0; i < l; i++) {
            temp[i] = x.charAt(i);
        }

        for (int i = 0; i < l; i++) {
            o[i] = temp[l - 1 - i];
        }

        String reverse = new String(o);
        boolean outcome;
        outcome = reverse.equals(x);
        System.out.println(outcome);
    }

    public static void Palindrome(double a) {
        String x = String.valueOf(a);
        x = x.replaceAll("[.]", "");
        int l = x.length();
        char[] temp = new char[l];
        char[] o = new char[l];

        for (int i = 0; i < l; i++) {
            temp[i] = x.charAt(i);
        }

        for (int j = 0; j < l; j++) {
            o[j] = temp[l - 1 - j];
        }

        String reverse = new String(o);
        boolean outcome;
        outcome = reverse.equals(x);
        System.out.println(outcome);
    }

    
  // 4. void method upConversion 
  public static boolean uConversion(byte v) {
        System.out.println();
        System.out.println("Short Data Type: " + ((short) v));
        System.out.println("Int Data Type: " + ((int) v));
        System.out.println("Long Data Type: " + ((long) v));
        System.out.println("Float Data Type: " + ((float) v));
        System.out.println("Double Data Type:" +((double) v));

        System.out.println();
        Short s = (short) v;
        System.out.println("Short object: " + s);
        Integer i = (int) v;
        System.out.println("Int object: " + i);
        Long l = (long) v;
        System.out.println("Long object: " + l);
        Float f = (float) v;
        System.out.println("Float object: " + f);
        Double d = (double) v;
        System.out.println("Double object: " + d);
        return true;
    }

  //5. downConversion void method
    public static boolean dConversion(double v) {
        System.out.println();
        System.out.println("Short Data Type: "+((short) v));
        System.out.println("Int Data Type: " +((int) v));
        System.out.println("Long Data Type: " +((long) v));
        System.out.println("Float Data Type: " +((float) v));
        System.out.println("Double Data Type: "+((double) v));

        System.out.println();
        Short s = (short) v;
        System.out.println("Short object:" + s);
        Integer i = (int) v;
        System.out.println("Object: " + i);
        Long l = (long) v;
        System.out.println("Long object: " + l);
        Float f = (float) v;
        System.out.println("Float object: " + f);
        Double d = (double) v;
        System.out.println("Double object:" + d);
        return true;
    }
  
  
}

    
   