/*
 * Test Program for MyString.java Assignment #2
 * 
 */
package ass2;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class Test {
     public static void main(String[] args) {
        MyString.isSubString("cat","the cat in the hat."); // look for string cat in longer string
        MyString.isSubString("bat","the cat in the hat."); //look for string bat in longer string 
       
        
        MyString aString = new MyString("the cat in the hat."); //for non static
        aString.isSubString("cat");
        aString.isSubString("the cat");
        
        //boolean found = new MyString("the cat in the hat.").isSubString("at.");
        
        aString.isSubString("bat");
        aString.isSubString("mouse");
        aString.isSubString("betty");
       
        //will bring out True if object or strng is a Palindrome else print false
        MyString.Palindrome("12321");
        MyString.Palindrome(12321);
        MyString.Palindrome(123.21);
        MyString.Palindrome("bob");
        MyString.Palindrome("level");
        MyString.Palindrome("mother");
        
        byte v = 10;
        double v2 = 10.0;
        MyString.uConversion(v);
        MyString.dConversion(v2);
    }
    
}
