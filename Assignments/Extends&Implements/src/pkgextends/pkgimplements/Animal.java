/*
 * Novembre 2nd, 2015
 *  Extends and Implements program
 */
package pkgextends.pkgimplements;

/**
 *
 * @author Tunbosun Olaniyi
 */
public abstract class Animal implements Pet {

    protected int legs;

    protected Animal(int l) {
        legs = l;
    }

   public abstract void eat();

    public void walk() {
        System.out.println("The animal walks with:" + legs + "legs");
    }

}
/*public static void main(String[] args) {
 // TODO code application logic here
 } */
