/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author michaelangelo
 */
public class FileIO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a file name:  ");
        String name = sc.nextLine(); //enter a whole line as the file name
        name = name + ".txt";

        File outFile = new File(name);

        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter(outFile));
        } catch (IOException err) {
            System.out.println("There was a problem:" + err.getMessage());
            System.exit(1);
        }
        String line = "";
        System.out.print("Enter another line:");
        do {
           
            line = sc.nextLine();
            if (!line.equalsIgnoreCase("exit")) {
                pw.println(line);
            }
        } while (!line.equalsIgnoreCase("exit"));
        pw.close();
    }
}
