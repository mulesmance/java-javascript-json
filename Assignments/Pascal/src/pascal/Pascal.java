/*
 * Pascal Triangle: this program will print 9 rows for the pascal triangle
 * By: Tunbosun Olaniyi
 * Oct 27th, 2015
 */
package pascal;
import java.util.Scanner;

public class Pascal {
public static final int numRow = 9;
 Scanner sc= new Scanner(System.in);
 int y = sc.nextInt();

private static int maximum = 0;

public static void main(String[] args) {
  
    
    
    int[][] pascal = new int[numRow + 1][];
    pascal[1] = new int[1+2];
    pascal[1][1] = 1;
    for (int n = 2; n <= numRow; n++) {
        pascal[n] = new int[n + 2];
        for (int col = 1; col < pascal[n].length - 1; col++) {
            pascal[n][col] = pascal[n - 1][col - 1] + pascal[n-1][col];
            String str = Integer.toString(pascal[n][col]);
            int l = str.length();
            if (l > maximum)
            maximum = l;
    
     } 
    }
     // print and format the triangle 
 for (int n = 1; n <= numRow; n++) {
      for (int s = numRow; s > n; s--)
           System.out.format("%-" + maximum + "s", " ");
               for (int col = 1; col < pascal[n].length - 1; col++) 
                 System.out.format("%-" + (maximum + maximum) + "s", pascal[n][col]);
                System.out.println();
        }
           
}
}

