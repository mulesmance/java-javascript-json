/*
 * November 5th, 2015
 * Exceptions
 */
package exception;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class Exceptions {

    public static void main(String[] args) {

        int[] data = new int[10];

        try {
            for (int i = 0; i < 10; i++) {
                data[i] = (int) (Math.random() * 1000);
                /*data[i] = new Double(Math.random()).intValue();   another way */
            }

        } catch (Exception a) {
            System.out.println("we had an exception - sorry!");
            System.out.println(a.toString());

        }

        Scanner sc = new Scanner(System.in);

        System.out.print("enter the index (-1 to end):");
        //  for (int i=next(sc); i >= 0; i = next(sc)) {
        while (true) {
            try {
                System.out.printf("enter the index (-1 to end):");
                int i = next(sc);
                if (i < 0) {
                    break;
                }

                System.out.println("The value at" + i + "is" + data[i]);
            } catch (ArrayIndexOutOfBoundsException obj) {
                System.out.println("sorry that is not in the proper range");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                sc.nextLine();
            }
        }
    }

    public static int next(Scanner s) throws Exception {
        try {
            return s.nextInt();

        } catch (InputMismatchException mis) {
            throw new Exception("Bad intput - Digits only!");
        }
    }
}
