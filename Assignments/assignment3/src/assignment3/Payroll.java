/*
 pay roll interface
 */
package assignment3;

/**
 *
 * @author Tunbosun Olaniyi
 */
public interface Payroll {

    public double calculatePay();

    public void setPayDetials();

}
