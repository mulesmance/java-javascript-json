/* class commision worker program */
package assignment3;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class CommissionWorker extends Employee {

    double salary;
    double percentage;
    double sales;

    public CommissionWorker(int empNumber, String name, double salary, double percentage) {
        this.name = name;
        this.empNumber = empNumber;
        this.salary = salary;
        this.percentage = percentage;

    }

    public double getCommission() {
        return salary + (sales * percentage);
    }

    public void setCommission() {
        System.out.println("what is the new salary?");
        salary = sc.nextDouble();
        sales = sc.nextDouble();
        percentage = sc.nextDouble();
    }

    public double calculatePay() {
        return salary + (percentage * sales);
    }

    public void setPayDetials() {
    }

    @Override
    public String toString() {
        String str = "CommissionWorker:  " + name + "\n";
        return str;
    }
}
