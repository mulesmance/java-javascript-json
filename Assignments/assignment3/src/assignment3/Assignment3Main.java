/*Test program for assignment*/
package assignment3;

import java.util.Scanner;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class Assignment3Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String name;
        int empNumber;
        double salary;
        double percentage;
        double sales;
        int piecesProduced;
        double pricePerPiece;
        double hours;
        double hourlyRate;
        double overTime;

        //Manager 
        System.out.println("Manager's name:");
        name = sc.next();

        System.out.println("Employee's number:");
        empNumber = sc.nextInt();

        System.out.println("Enter your salary:");
        salary = sc.nextDouble();

        Manager emp = new Manager(name, empNumber, salary);
        System.out.println(emp.toString());
        System.out.println("Employee's pay is:" + emp.getSalary());
        System.out.println(emp.name + "'s pay is:" + emp.getSalary());
        emp.setSalary();
        System.out.println(emp.name + "'s new pay is:" + emp.getSalary());

        // Commissioner
        System.out.println("CommissionWorker's name:");
        name = sc.next();
        System.out.println("CommissionWorker's number:");
        empNumber = sc.nextInt();
        System.out.println("Salary:");
        salary = sc.nextDouble();
        System.out.println("percentage:");
        percentage = sc.nextDouble();
        System.out.println("sales:");
        sales = sc.nextDouble();

        CommissionWorker emp2 = new CommissionWorker( empNumber,  name,  salary,  percentage);
        System.out.println(emp2.toString());
        System.out.println("Employee's pay is " + emp2.getCommission());
        System.out.println(emp2.name + "'s pay is: " + emp2.getCommission());
        emp2.setCommission();
        System.out.println(emp2.name + "'s new pay is: " + emp2.getCommission());

        //PieceWorker
        System.out.println("PieceWorker's name:");
        name = sc.next();
        System.out.println("PieceWorker's number:");
        empNumber = sc.nextInt();
        System.out.println("Pieces Produced:");
        piecesProduced = sc.nextInt();
        System.out.println("Price per piece:");
        pricePerPiece = sc.nextDouble();

        Pieceworker emp3 = new Pieceworker(empNumber, name, salary, pricePerPiece);
        System.out.println(emp3.toString());
        System.out.println("Employee's pay is: " + emp3.getPiece());
        System.out.println(emp3.name + "'s pay is: " + emp3.getPiece());
        emp3.setPiece();
        System.out.println(emp3.name + "'s new pay is: " + emp3.getPiece());

        //Hourlyworker
        System.out.println("Hourlyworker Employee's name:");
        name = sc.next();
        System.out.println("Hourlyworker Employee's number:");
        empNumber = sc.nextInt();
        System.out.println("Hours worked:");
        hours = sc.nextInt();
        System.out.println("Pay per hour:");
        hourlyRate = sc.nextDouble();
        System.out.println("Total Overtime:");
        overTime = sc.nextDouble();

        HourlyWorker emp4 = new HourlyWorker(empNumber, name, hourlyRate, overTime, hours);
        System.out.println(emp4.toString());
        System.out.println("Employee's pay is: " + emp4.getHour());
        System.out.println(emp3.name + "'s pay is: " + emp4.getHour());
        emp4.setHour();
        System.out.println(emp3.name + "'s new pay is: " + emp4.getHour());

    }

}
