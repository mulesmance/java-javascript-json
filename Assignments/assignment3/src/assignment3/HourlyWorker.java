package assignment3;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class HourlyWorker extends Employee {

    double hours;
    double hourlyRate;
    double overTime;

    public HourlyWorker(int empNumber, String name, double hourlyRate, double overTime, double hours) {

        this.empNumber = empNumber;
        this.name = name;
        this.hourlyRate = hourlyRate;
        this.hours = hours;
        this.overTime = overTime * 1.5;
    }

    public double getHour() {
        return (hours * hourlyRate) + (overTime * hourlyRate);
    }

    public void setHour() {
        System.out.println("what is the new salary?");
        hours = sc.nextDouble();
        hourlyRate = sc.nextDouble();
        overTime = sc.nextDouble();

    }

    // @Override

    public double calculatePay() {
        return (hours * hourlyRate) + overTime;
    }
//        @Override

    public void setPayDetials() {
    }

    @Override
    public String toString() {
        String str = "HourlyWorker:  " + name + "\n";
        return str;
    }
}
