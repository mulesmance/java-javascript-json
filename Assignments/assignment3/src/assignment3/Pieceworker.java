package assignment3;

/**
 * @author Tunbosun Olaniyi
 */
public class Pieceworker extends Employee {

    int piecesProduced;
    double pricePerPiece;

    public Pieceworker(int empNumber, String name, double salary, double pricePerPiece) {
        this.empNumber = empNumber;
        this.name = name;
        this.pricePerPiece = pricePerPiece;

    }

    public double getPiece() {
        return (piecesProduced * pricePerPiece);
    }

    public void setPiece() {
        System.out.println("what is the new salary?");
        piecesProduced = sc.nextInt();
        pricePerPiece = sc.nextDouble();
    }

    public double calculatePay() {
        return piecesProduced * pricePerPiece;
    }

    public void setPayDetials() {
    }

    @Override
    public String toString() {
        String str = "Pieceworker:  " + name + "\n";
        return str;
    }
}
