/*Manager class */
package assignment3;

/**
 *
 * @author Tunbosun Olaniyi
 */
public class Manager extends Employee {

    public double salary;

    Manager(String name,int empNumber,  double salary) {
        this.salary = salary;
        this.empNumber = empNumber;
        this.name = name;
    }

    /* Manager(String name, int empNumber, double salary) {
     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }*/
    public double getSalary() {
      
        return this.salary;
    }

    public void setSalary() {
        System.out.println("what is the new salary?");
        this.salary = sc.nextDouble();
    }

    //@Override
    public double calculatePay() {
        return this.salary;
    }

    //@Override 
    public void setPayDetials() {
    }

    //toString method 
    @Override
    public String toString() {
        String str = "Manager:  " + name + "\n";
        return str;
    }

}
